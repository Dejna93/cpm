package com.company;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Damian on 2016-03-09.
 */
public class Proces {

    private int id;
    private String name;
    private double earlyStart;
    private double earlyFinish;
    private double lateStart;
    private double lateFinish;


    private List<Integer> prevProcess = new ArrayList<>();
    private List<Integer> succProcess = new ArrayList<>();
    public Proces() {
    }

    public Proces(int id, String name, double earlyStart, double earlyFinish, double lateStart, double lateFinish) {
        this.name = name;
        this.earlyStart = earlyStart;
        this.earlyFinish = earlyFinish;
        this.lateStart = lateStart;
        this.lateFinish = lateFinish;
        this.id = id;
    }

    public void addPrevProces(int id){
        if(id != this.id){
            prevProcess.add(id);
        }
    }
    public void addSuccProces(int id){
        if(id != this.id){
            succProcess.add(id);
        }
    }


    public int nextId(){
        return ++id;
    }




    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Proces)) return false;

        Proces proces = (Proces) o;

        if (Double.compare(proces.getEarlyStart(), getEarlyStart()) != 0) return false;
        if (Double.compare(proces.getEarlyFinish(), getEarlyFinish()) != 0) return false;
        if (Double.compare(proces.getLateStart(), getLateStart()) != 0) return false;
        if (Double.compare(proces.getLateFinish(), getLateFinish()) != 0) return false;
        return getName() != null ? getName().equals(proces.getName()) : proces.getName() == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = getName() != null ? getName().hashCode() : 0;
        temp = Double.doubleToLongBits(getEarlyStart());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getEarlyFinish());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getLateStart());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getLateFinish());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Proces{" +
                "name='" + name + '\'' +
                ", earlyStart=" + earlyStart +
                ", earlyFinish=" + earlyFinish +
                ", lateStart=" + lateStart +
                ", lateFinish=" + lateFinish +
                '}';
    }
    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getEarlyStart() {
        return earlyStart;
    }

    public void setEarlyStart(double earlyStart) {
        this.earlyStart = earlyStart;
    }

    public double getEarlyFinish() {
        return earlyFinish;
    }

    public void setEarlyFinish(double earlyFinish) {
        this.earlyFinish = earlyFinish;
    }

    public double getLateStart() {
        return lateStart;
    }

    public void setLateStart(double lateStart) {
        this.lateStart = lateStart;
    }

    public double getLateFinish() {
        return lateFinish;
    }

    public void setLateFinish(double lateFinish) {
        this.lateFinish = lateFinish;
    }
}
