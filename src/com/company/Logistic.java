package com.company;

import sun.rmi.runtime.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Damian on 2016-03-09.
 */
public class Logistic {

    private List<Proces>procesList = new ArrayList<>();
    private States state = States.START ;
    private  Scanner scanner = new Scanner(System.in);

    public Logistic() {
    }

    public  void runApplication(){

            inputHandler();

    }
    public void printMenu(){
        System.out.println("MENU Aplikacji CPM - AGH");
        System.out.println("1. Dodanie procesów ");
        System.out.println("2. Połączenie procesów");
        System.out.println("3. Obliczenie ścieżki krytycznej");
        System.out.println("4. Wydruk ");
        System.out.println("5. EXIT");
    }
    public synchronized void inputHandler(){

        while(state != States.EXIT) {
            printMenu();

            switch (scanner.nextInt()) {
                case 1:
                    initProcess();
                    break;
                case 2:
                    buildRelactions();
                    break;
                case 3:
                    calculateCPM();
                    break;
                case 4:
                    printCPM();
                    break;
                case 5:
                    exitProgram();
                    break;


            }
        }
    }

    private void buildRelactions() {
        if(state == States.ENDBUILD && procesList != null){
            //printLIST
            /*
            wybierz proces i ustaw mu id do suc i prev
            
             */
        }
    }

    private void exitProgram() {
        state = States.EXIT;
    }

    private void printCPM() {
        System.out.println("PRINT");
        for(int i = 0 ; i < procesList.size() ; i++){
            System.out.println(procesList.get(i).toString());
        }
    }

    private void calculateCPM() {
        if(state == States.BUILD){

        }else{
            System.err.println("Błąd nie można wykonać obliczeń bez diagramu");
                inputHandler();


        }
    }

    public void initProcess(){
        //Inicjalizacja procesu tylko na poczatku dla ochrony przed niepowołoanym rozpoczęciem
        //
        while(state == States.START || state == States.BUILD){
            System.out.println("Dodać nowy proces? \n Nie - nastepne pytanie");
            procesList.add(createProces(procesList.size()));
            if(ask() == true){
                state= States.ENDBUILD;
            }
        }
       /* if (state == States.START || state == States.BUILD){
            state = States.BUILD;
            while (state != States.ENDBUILD){
                System.out.println("Dodać nowy proces? \n Nie - nastepne pytanie");
                if(ask() == true){

                    if(addProces() != -1){
                        System.out.println("Dodano proces ");
                    }else{
                        System.out.println("Bład dodania");
                    }

                }else{
                    System.out.println("Podanie potomnego procesu?");
                    if(ask() == true){
                        addProces(procesList.size() - 1 );
                    }else {
                        state = States.ENDBUILD;
                    }
                }

            }

        }
       else{
            System.err.println("Czy chcesz na nowo utworzyć diagram procesów?");
            System.out.println("Tak/Nie");
            if( ask()== true ){
                state =States.START;
                initProcess();
            }else{

                    inputHandler();
            }

        }*/

    }

    private void addProces(int i) {

    }
    private void addingProcedure(){
        state = States.ADDCHILD;
        int id = addProces();
        while(state == States.ADDCHILD) {
            if (ask() == true) {
                procesList.get(id).addSuccProces(addProces());
                getLastProces().addPrevProces(id);
            }else{
                state = States.BUILD;
            }
        }
    }

    private int addProces(){
        int id = procesList.size();
        System.out.println("Dodanie nowego procesu według schematu : \n Nazwa,wczesnyStart (d) , wczesnyKoniec(d) , poznyStart(d), poznyKoniec(d) ENTER \n (d) - liczba ");

        procesList.add(createProces(id));
        return id;
    }

    private Proces getLastProces(){
        if(procesList!= null){
            return procesList.get(procesList.size() - 1 );
        }
        return null;
    }
    private Proces createProces(int id){
        List<String> toProcess = handleProces();
        if(toProcess.size() == 5)
        {
            return new Proces(id
                    , toProcess.get(0)
                    , Double.parseDouble(toProcess.get(1))
                    , Double.parseDouble(toProcess.get(2))
                    , Double.parseDouble(toProcess.get(3))
                    , Double.parseDouble(toProcess.get(4))
            );
        }
        return null;
    }
    private boolean ask(){
        System.out.println("Tak/Nie");
        if(scanner.next().equals("Tak"))
            return true;
        else
            return false;
    }
    private List<String> handleProces(){
        List<String> lists =new ArrayList<>();
        System.out.print("Nowy proces ");
        for( int i=0 ;( i < 5 ) && (scanner.hasNext()); i++){

            lists.add(scanner.next());

        }
        System.out.println(lists.toString());
        return lists;
    }
}
