package com.company;

/**
 * Created by Damian on 2016-03-14.
 */
public enum States {
    START , BUILD , ADDCHILD , ENDBUILD, CALCULATE , EXIT
}
